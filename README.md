About this software
===================

This software powers the cfarm compile farm infrastructure, it runs live at <https://portal.cfarm.net/>.

It has the following features:

- user management: users can request an account, which must be approved by an administrator
- SSH key management: users can update their SSH keys themselves
- machine management: machines are polled with Ansible to gather technical information such
  as CPU, RAM, disk, OS, etc.  It is then displayed so that users can find a suitable machine
  for their tasks.
- automated deployment: user accounts and SSH keys are (semi-)automatically deployed on
  all machines, using again Ansible.

The software is released under the GNU Affero GPL 3.0 license.  See `COPYING` for details.

Installation
============

There are a few required build dependencies:

    $ sudo apt install python3-dev librrd-dev

Then, the simplest way is to use a virtualenv to install the python dependencies:

    $ sudo apt install python3-virtualenv
    $ python3 -m venv ~/venv3-gccfarm
    $ . ~/venv3-gccfarm/bin/activate
    (venv3-gccfarm) $ pip install -r requirements/develop.txt

Note that the project only supports Python 3.

Configuration
=============

To change the configuration locally, create `gccfarm/local_settings.py` and
simply override the settings from `gccfarm/settings.py` you want to change.

This application defines several settings:

- `CFARM_FIRST_UID`: minimum UID to be used when assigning an UID to a new user.
- `CFARM_FORBIDDEN_USERNAMES`: list of forbidden user names (for instance,
  user names such as `root` should be forbidden to avoid overriding the root
  account on farm machines).
- `CFARM_FORBIDDEN_UID`: similarly, list of forbidden UID.

Creating users
==============

Users should fill in the "join" form to request an account.  This will create
the user in the Django database, but with a "non-approved" state.  An admin can
then approve the account creation, which will allocate an UID for the new user.

It is possible to create an approved account manually:

    (venv3-gccfarm) $ ./manage.py create_farmuser <login> <uid> <email>

Be careful when choosing the UID!  The newly created user will be deployed
on all machines as soon as the ansible tasks are next run.

In both cases, the new user can define his/her password by clicking the
"forgot my password" button on the login page and entering his/her email address.


Deploying users and SSH keys
============================

We use ansible to deploy Unix users and SSH keys to farm machines.

All data is extracted directly from the Django database: the list of machines
to deploy to, the list of users to create, and the SSH keys to deploy.
Of course, the local Unix user doing the deployment must have SSH access to the
farm machines (as root).

Note: only users that satisfy the following conditions will be deployed:

- the user has an UID
- the user has been approved by an admin
- the user is "active" (in the Django sense, i.e. it can login to the website)

Unix users are never removed automatically on farm machines.
For SSH keys, any key previously existing in the `~/.ssh/authorized_keys` file
of a user is overridden during deployment.

Manual deployment
-----------------

To deploy for a given user, use the following commands.  By default,
all commands run in "dry-run" mode, which means they will only print out
what they would do, but not actually apply the changes:

    (venv3-gccfarm) $ ./manage.py deploy_users <username> [username [...]]
    (venv3-gccfarm) $ ./manage.py deploy_sshkeys <username> [username [...]]

To actually make the changes, pass the `--apply` flag.

The commands are self-documented (use `--help`).  In particular, you can
deploy for *all* users, and you can also restrict deployment to a subset of
machines.

Automated deployment
--------------------

Another set of commands is used to automate deployment.  They basically track
what has changed in the Django database (new user, new or modified SSH key),
and only deploy changes for the relevant users.

To create missing Unix users on all machines:

    (venv3-gccfarm) $ ./manage.py deploy_latest_users
    (venv3-gccfarm) $ ./manage.py deploy_latest_users --apply

To deploy changed SSH keys on all machines:

    (venv3-gccfarm) $ ./manage.py deploy_sshkeys_from_queue
    (venv3-gccfarm) $ ./manage.py deploy_sshkeys_from_queue --apply

To keep trying to deploy SSH keys on unreachable machines:

    (venv3-gccfarm) $ ./manage.py deploy_sshkeys_from_backlog
    (venv3-gccfarm) $ ./manage.py deploy_sshkeys_from_backlog --apply

Of course, you may want to run these commands as a cron task.  Deploying
SSH keys from the queue can be done fairly often, for instance every 15 minutes,
because it does not do anything if the queue is empty.  On the other hand, deploying
users and SSH keys from the backlog will keep trying to connect to unreachable
or failed machines, so it should be done e.g. once or twice a day.

TODO: logging.

Gathering information on machines
=================================

Ansible is also used to gather technical information about farm machines.

To run the gathering process on all registered machines:

    (venv3-gccfarm) $ ./manage.py gather_info

This will gather a lot of technical information and save it in the database.
Obviously, this command should be run in a cron job regularly, for instance
every night.

You can also pass a list of hostnames on the command line to limit the process
to a subset of machines (these machines must exist in the Django database).
