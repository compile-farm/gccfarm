# -*- coding: utf-8 -*-
from __future__ import unicode_literals, division, print_function

from datetime import datetime

from django.contrib.syndication.views import Feed
from django.urls import reverse
from django.utils.html import linebreaks

from .models import Article


class RSS(Feed):
    title = "The cfarm compile farm"
    link = "/"
    description = "The cfarm project maintains machines of various architectures and provides SSH access to free software developers."

    def items(self):
        # Return the 15 latest news articles
        return Article.articles.all()[:15]

    def item_title(self, item):
        return item.title

    def item_description(self, item):
        return linebreaks(item.content)

    def item_pubdate(self, item):
        return datetime.combine(item.pub_date, datetime.min.time())

    # item_link is only needed if NewsItem has no get_absolute_url method.
    def item_link(self, item):
        return reverse('news_detail', args=[item.pk])

