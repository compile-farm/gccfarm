from django.conf import settings
from django.core import mail
from django.template import loader


def send_templated_mail(context, subject_template_name, email_template_name,
                        from_email, recipient_list):
    subject = loader.render_to_string(subject_template_name, context)
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    body = loader.render_to_string(email_template_name, context)
    m = mail.EmailMessage(
        subject=subject,
        body=body,
        from_email=from_email,
        to=recipient_list,
    )
    reply_to = settings.CFARM_EMAIL_REPLY_TO
    if reply_to:
        m.reply_to = [reply_to]
    m.send()


def send_templated_mail_managers(context, subject_template_name, email_template_name,
                                 fail_silently=False):
    if not settings.MANAGERS:
        return
    subject = loader.render_to_string(subject_template_name, context)
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())
    body = loader.render_to_string(email_template_name, context)
    m = mail.EmailMessage(
        subject='%s%s' % (settings.EMAIL_SUBJECT_PREFIX, subject),
        body=body,
        from_email=settings.SERVER_EMAIL,
        to=[a[1] for a in settings.MANAGERS],
    )
    reply_to = settings.CFARM_EMAIL_REPLY_TO
    if reply_to:
        m.reply_to = [reply_to]
    m.send(fail_silently=fail_silently)
