# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.db import models
from easy_select2.widgets import Select2Multiple, Select2

from .models import Machine


class MachineAdmin(admin.ModelAdmin):
    fields = ['name',
              'shared', 'managed_users',
              'retired', 'retired_since',
              'hostname', 'description', 'ssh_ports', 'ssh_configuration',
              ('hoster', 'location'),
              'project',
              'ssh_admin_user', 'python_interpreter', 'home_directory',
              ('uid_offset', 'largest_deployed_uid'),
              ('manual_cpu_count', 'nb_cpu', 'nb_cores', 'nb_threads'),
              'processor',
              ('success', 'last_success'),
              ('last_stats', 'cpu_usage', 'memory_usage', 'disk_io_usage')]
    readonly_fields = ['success', 'last_success', 'retired_since', 'last_stats', 'cpu_usage', 'memory_usage', 'disk_io_usage']
    list_display = ['name', 'shared', 'managed_users', 'retired', 'ssh_ports', 'hoster', 'location', 'uid_offset', 'largest_deployed_uid', 'success', 'last_success']
    list_filter = ['shared', 'managed_users', 'retired', 'success', 'hoster', 'location', 'project', 'largest_deployed_uid', 'uid_offset']
    search_fields = ['name', 'hoster__name', 'location', 'ssh_ports', 'ssh_configuration', 'project__name', 'description']

    formfield_overrides = {
        models.ManyToManyField: {'widget': Select2Multiple},
        models.ForeignKey: {'widget': Select2},
    }

admin.site.register(Machine, MachineAdmin)
