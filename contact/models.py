# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models


class AbstractContact(models.Model):
    """Describes a contact point to a person or entity, for a specific purpose."""
    last_updated = models.DateTimeField(auto_now=True, editable=False)
    name = models.CharField(max_length=64,
                            verbose_name='name',
                            help_text='Name of the person or entity',
                            blank=False)
    email = models.EmailField(verbose_name='email address',
                              help_text='Optional',
                              blank=True)
    url = models.URLField(verbose_name='Web URL',
                          help_text='Optional',
                          blank=True)
    purpose = models.CharField(max_length=128,
                               verbose_name='purpose',
                               help_text='When and why this contact should be used: support, hardware delivery, general questions...',
                               blank=False)
    contact_info = models.TextField(verbose_name='other contact information',
                                    help_text='Phone number, physical address...',
                                    blank=True)
    notes = models.TextField(verbose_name='notes',
                             help_text='Additional information: date of last contact, context...',
                             blank=True)

    def _email_or_url(self):
        return self.email if self.email else self.url
    _email_or_url.short_description = "Email/URL"
    email_or_url = property(_email_or_url)

    def __str__(self):
        return self.name

    class Meta:
        abstract = True


class HosterContact(AbstractContact):
    hoster = models.ForeignKey("Hoster", on_delete=models.SET_NULL,
                               blank=True, null=True,
                               related_name="contacts")

    class Meta:
        ordering = ['hoster', '-last_updated']


class Hoster(models.Model):
    """Administrative entity that hosts one or more machines for the compile
    farm.  Used to track contact information.
    """
    name = models.CharField(max_length=64,
                            verbose_name='name',
                            help_text='Name of the hosting organization',
                            blank=False)
    website = models.URLField(verbose_name='Website URL',
                              help_text='Will be displayed publicly if set',
                              blank=True)
    public = models.BooleanField(verbose_name='public?',
                                 help_text='Should the name of the hoster be displayed publicly?',
                                 default=True)
    notes = models.TextField(verbose_name='notes',
                             help_text='Additional private information: details of relationship, technical setup, IP addresses, admin access...',
                             blank=True)


    def _get_machines_count(self):
        return self.machines.count()
    _get_machines_count.short_description = "hosted machines"
    nb_machines = property(_get_machines_count)

    def _get_contacts_count(self):
        return self.contacts.count()
    _get_contacts_count.short_description = "contacts"
    nb_contacts = property(_get_contacts_count)

    def __str__(self):
        return self.name

    class Meta:
        # TODO: use Lower('name') when we get to Django 2.X
        # https://stackoverflow.com/questions/47348280/django-model-meta-how-to-have-default-ordering-case-insensitive
        ordering = ['name']


class ProjectContact(AbstractContact):
    project = models.ForeignKey("Project", on_delete=models.SET_NULL,
                                blank=True, null=True,
                                related_name="contacts")

    class Meta:
        ordering = ['project', '-last_updated']


class Project(models.Model):
    """Project that uses a set of machines, often exclusively.  Used to track
    contact information.
    """
    name = models.CharField(max_length=64,
                            verbose_name='name',
                            help_text='Name of the project',
                            blank=False)

    def _get_machines_count(self):
        return self.machines.count()
    _get_machines_count.short_description = "machines used"
    nb_machines = property(_get_machines_count)

    def _get_contacts_count(self):
        return self.contacts.count()
    _get_contacts_count.short_description = "contacts"
    nb_contacts = property(_get_contacts_count)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
