# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from django.db import models
from easy_select2.widgets import Select2Multiple, Select2

from .models import HosterContact, Hoster, ProjectContact, Project


class ContactAdmin(admin.ModelAdmin):
    readonly_fields = ['last_updated']
    search_fields = ['name', 'purpose', 'contact_info', 'notes']
    formfield_overrides = {
        models.ManyToManyField: {'widget': Select2Multiple},
        models.ForeignKey: {'widget': Select2},
    }

class HosterContactAdmin(ContactAdmin):
    list_display = ['name', 'email_or_url', 'hoster', 'purpose', 'last_updated']
    list_filter = ['hoster']

class HosterContactInline(admin.StackedInline):
    model = HosterContact
    extra = 1

class ProjectContactAdmin(ContactAdmin):
    list_display = ['name', 'email_or_url', 'project', 'purpose', 'last_updated']
    list_filter = ['project']

class ProjectContactInline(admin.StackedInline):
    model = ProjectContact
    extra = 1


class HosterAdmin(admin.ModelAdmin):
    list_display = ['name', 'website', 'public', 'nb_machines', 'nb_contacts']
    list_filter = ['public']
    search_fields = ['name', 'website']
    inlines = [HosterContactInline]


class ProjectAdmin(admin.ModelAdmin):
    list_display = ['name', 'nb_machines', 'nb_contacts']
    search_fields = ['name']
    inlines = [ProjectContactInline]


admin.site.register(HosterContact, HosterContactAdmin)
admin.site.register(Hoster, HosterAdmin)
admin.site.register(ProjectContact, ProjectContactAdmin)
admin.site.register(Project, ProjectAdmin)
