# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.db.models import Max
from django.core import validators
from django.conf import settings
from django.utils import timezone
from django.utils.deconstruct import deconstructible
from django.contrib.auth.models import AbstractUser
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ValidationError
import simplesshkey.util

from gccfarm_machine.models import Machine


def validate_allowed_username(username):
    if username in settings.CFARM_FORBIDDEN_USERNAMES:
        raise ValidationError(
            _("'%(username)s' is a reserved username, please choose another one."),
            params={'username': username},
        )


class FarmUser(AbstractUser):
    # Override fields from AbstractUser (stricter constraints)
    username = models.CharField(
        _('username'),
        max_length=16,
        unique=True,
        help_text=_('Required. 16 characters or fewer.'),
        validators=[validators.RegexValidator(r'^[a-z][a-z0-9_-]*$',
                                              'Enter a valid Unix login. It must start with a lowercase letter and may only contain lowercase letters, numbers, dash and underscore.'),
                    validate_allowed_username],
        error_messages={
            'unique': _("A user with that username already exists."),
        },
     )
    first_name = models.CharField(_('first name'), max_length=150)
    last_name = models.CharField(_('last name'), max_length=150, blank=True)
    email = models.EmailField(_('email address'))
    # New fields
    uid = models.PositiveIntegerField('UID',  unique=True, null=True, blank=True,
                                      help_text='Unix UID that will be deployed on the farm machines')
    contributions = models.TextField(help_text="Free software contributions")
    request_date = models.DateTimeField(auto_now_add=True, editable=False,
                                        help_text="Date at which the account was requested")
    # This is currently only used when deploying SSH keys from the queue.
    # It makes no sense to try to automatically deploy SSH keys for a user
    # if the user has not yet been created on any machine.
    deployment_started = models.BooleanField(default=False, editable=False,
                                             help_text="Whether this user has been deployed on at least one machine")
    # This defaults to True, because we want accounts created by the
    # 'createsuperuser' command or manually in the admin to be
    # automatically approved.  The "normal" process of requesting a farm
    # user account will set this to False as expected.
    approved = models.BooleanField(default=True, editable=False,
                                   help_text="Whether this account request has been approved")
    reject_reason = models.TextField(editable=False, blank=True,
                                     help_text="Reason for rejecting this account creation request")
    approved_by = models.ForeignKey(settings.AUTH_USER_MODEL, null=True, editable=False,
                                    related_name="moderated_farm_users", on_delete=models.SET_NULL,
                                    verbose_name="moderated by",
                                    help_text="Administrator that approved or rejected this account's creation")
    approved_date = models.DateTimeField(editable=False, null=True,
                                         verbose_name="moderated date",
                                         help_text="Date at which this account was approved or rejected by an admin")

    def is_deployable(self):
        return self.uid != None and self.approved and self.is_active

    def is_forbidden(self):
        """Check whether this user has a forbidden user name or UID, based on the
        settings CFARM_FORBIDDEN_USERNAMES and CFARM_FORBIDDEN_UID."""
        if self.username in settings.CFARM_FORBIDDEN_USERNAMES:
            return True
        if self.uid == None:
            return False
        return self.uid in settings.CFARM_FORBIDDEN_UID

    def is_allowed(self):
        return not self.is_forbidden()

    def get_sshkeys(self):
        """Returns all SSH keys associated to this user as a string, suitable for
        use as an authorized_keys file.  Each key is separated by a
        newline.

        When several keys have the same content, only one of them is
        returned (even if the keys have different comments).

        In addition, keys are sorted to ensure that the order is fixed.

        If the user is disabled (is_active set to False), simply returns a
        comment saying so.
        """
        if not self.is_active:
            return "# This user is disabled, please contact farm administrators for details"
        # Extract key content of each key and use it to index a dict, to
        # ensure uniqueness.
        keys = {simplesshkey.util.pubkey_parse(userkey.key).keydata: userkey.key for userkey in self.userkey_set.all() if userkey.key != ''}
        return "\n".join(sorted(keys.values()))

    def nb_machines_synced(self):
        return Machine.managed_machines.count() - self.sshkey_deploy_backlog.count()

    def nb_machines_total(self):
        """Returns the total number of machines.  This is just a convenience
        method for use in templates."""
        return Machine.managed_machines.count()

    @classmethod
    def all_deployable(cls):
        return cls.objects.filter(uid__isnull=False, approved=True, is_active=True)

    @classmethod
    def next_uid(cls):
        agg = cls.objects.aggregate(Max('uid'))
        max_uid = agg['uid__max']
        if max_uid == None:
            # No UID assigned yet
            return settings.CFARM_FIRST_UID
        else:
            return max(max_uid + 1, settings.CFARM_FIRST_UID)

    def approve(self, request):
        """Approve this user account, and allocate a new UID"""
        self.is_active = True
        self.approved = True
        self.approved_by = request.user
        self.approved_date = timezone.now()
        if self.uid == None:
            self.uid = self.next_uid()
        self.save()

    def reject(self, request, reject_reason):
        """Reject this account creation request"""
        self.is_active = False
        self.approved = False
        self.approved_by = request.user
        self.approved_date = timezone.now()
        self.reject_reason = reject_reason
        self.save()

    def save(self, *args, **kwargs):
        super(FarmUser, self).save(*args, **kwargs)
        # Check for inactive user (i.e. disabled) and add it to the queue
        # to remove SSH keys on farm machines.
        if self.uid != None and self.approved and not self.is_active:
            queueitem = SSHKeyDeployQueue(user=self)
            queueitem.save()

    def __str__(self):
        return self.username

    class Meta:
        ordering = ['uid']


# Models related to deployment on machines
class SSHKeyDeployQueue(models.Model):
    """Queue of users waiting for their SSH keys to be deployed.

    A user is added to this queue every time it touches a SSH key (add or
    remove), or when the account is disabled (in which case the deployment
    task will remove SSH keys on farm machines).  Once its SSH keys are
    deployed on most machines, the user is removed from this queue.

    A user can be present multiple times in this queue, but the SSH keys
    will only be deployed once.  If the user is added to this queue while
    a deployment is currently running, it will be picked up in the next
    deployment.

    """

    user = models.ForeignKey(FarmUser, on_delete=models.CASCADE,
                             related_name='sshkey_deploy_queue')
    added = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = verbose_name_plural = 'SSH key deploy queue'


class SSHKeyDeployBacklog(models.Model):
    """Second queue for SSH key deployment, to account for unreachable machines.

    A user is moved to this backlog queue when its SSH keys have been
    deployed on a large fraction of machines, but not all of them.

    The backlog queue tracks which machine is missing the updated SSH keys
    for each user.  A given couple (user, machine) can only appear once in
    the backlog.
    """

    user = models.ForeignKey(FarmUser, on_delete=models.CASCADE,
                             related_name='sshkey_deploy_backlog')
    machine = models.ForeignKey(Machine, on_delete=models.CASCADE,
                                related_name='sshkey_deploy_backlog')
    added = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = ('user', 'machine')
        index_together = ('user', 'machine')
        verbose_name = verbose_name_plural = 'SSH key deploy backlog'
