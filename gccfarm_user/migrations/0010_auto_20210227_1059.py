# -*- coding: utf-8 -*-
# Generated by Django 1.11.29 on 2021-02-27 10:59
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('gccfarm_user', '0009_auto_20210227_1054'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sshkeydeployqueue',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='sshkey_deploy_queue', to=settings.AUTH_USER_MODEL),
        ),
    ]
