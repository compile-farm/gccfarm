# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ValidationError

from gccfarm_user.models import FarmUser


class Command(BaseCommand):
    help = 'Print the date of each user registration (used to create a plot showing user registration over time)'

    def handle(self, *args, **options):
        for user in FarmUser.objects.filter(approved=True).order_by("request_date"):
            self.stdout.write(user.request_date.date().isoformat())
