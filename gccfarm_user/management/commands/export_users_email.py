# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ValidationError

from gccfarm_user.models import FarmUser


class Command(BaseCommand):
    help = 'Export the email address of all active farm users, one address per line'

    def handle(self, *args, **options):
        for email in sorted(set(user.email for user in FarmUser.all_deployable())):
            self.stdout.write(email)
