# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.management.base import BaseCommand, CommandError

import filelock

from ansible_farm import deploy


class Command(BaseCommand):
    help = 'Automatically deploy changed SSH keys on farm machines, using the queue of pending SSH keys.  Runs in dry-run mode by default.'

    def add_arguments(self, parser):
        parser.add_argument('--apply', action='store_true',
                            help="Actually apply the changes")

    def handle(self, *args, **options):
        lock = filelock.FileLock('/tmp/gccfarm-deploy-sshkeys.lock')
        lock.timeout = 0
        try:
            with lock:
                deploy.deploy_sshkeys_from_queue(not options['apply'])
        except filelock.Timeout:
            self.stderr.write('Another SSH keys deployment is already running')
