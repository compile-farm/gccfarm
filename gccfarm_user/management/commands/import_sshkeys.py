# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

import codecs
import os
from collections import defaultdict

from django.db import transaction
from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ValidationError

from simplesshkey.models import UserKey
from gccfarm_user.models import FarmUser


class Command(BaseCommand):
    help = 'Import SSH keys for existing users.  Keys must be organised as one dir for each user, with a file "authorized_keys" in each dir'

    def add_arguments(self, parser):
        parser.add_argument('directory', help='Root directory containing SSH keys')

    @transaction.atomic
    def handle(self, *args, **options):
        directory = options['directory']
        user_success = 0
        user_total = 0
        key_success = 0
        key_total = 0
        failed = defaultdict(list)
        for username in os.listdir(directory):
            print("== {} ==".format(username))
            keyfile = os.path.join(directory, username, 'authorized_keys')
            try:
                user_total += 1
                with codecs.open(keyfile, 'r', encoding='utf-8') as f:
                    try:
                        u = FarmUser.objects.get(username=username)
                    except FarmUser.DoesNotExist:
                        failed[username].append('User not found in database')
                        continue
                    user_success += 1
                    for (linenumber, line) in enumerate(set(f.readlines())):
                        key_total += 1
                        # Handle keys without comment
                        if line.count(' ') == 1:
                            key = UserKey(user=u, key=line, name='{}{}'.format(username, linenumber))
                        else:
                            key = UserKey(user=u, key=line)
                        try:
                            key.full_clean(exclude=['key'])
                        except ValidationError as e:
                            failed[username].append(e.message_dict)
                            continue
                        key_success += 1
                        print("Imported {}".format(key))
                        key.save()
            except IOError:
                failed[username].append('No authorized_keys found')
        self.stdout.write("User key import: {}/{} users successful, {}/{} keys successful".format(user_success, user_total,
                                                                                                  key_success, key_total))
        for (username, errors) in failed.items():
            self.stderr.write("Failure for user {}: {}".format(username, errors))
