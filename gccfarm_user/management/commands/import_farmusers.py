# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

import codecs
import re

from django.core.management.base import BaseCommand, CommandError
from django.core.exceptions import ValidationError

from gccfarm_user.models import FarmUser


class Command(BaseCommand):
    help = 'Import all farm users from the old database (simple text format)'

    def add_arguments(self, parser):
        parser.add_argument('filename', help='File containing user accounts to import')

    def handle(self, *args, **options):
        success = 0
        total = 0
        failed = dict()
        with codecs.open(options['filename'], 'r', encoding='utf-8') as f:
            # Parse file
            for line in f.readlines():
                match = re.match(r'^(?P<uid>\d+)\s+(?P<login>[\w_-]+)\s+(?P<name>[^<]+) <(?P<email>.*?)>\s+(?P<contrib>.*)$', line, re.UNICODE)
                if not match:
                    # Try to match without name
                    match = re.match(r'^(?P<uid>\d+)\s+(?P<login>[\w_-]+)\s+(?P<email>\S+)\s+(?P<contrib>.*)$', line, re.UNICODE)
                    if not match:
                        self.stderr.write("Failed to parse line: {}".format(line))
                        continue
                total += 1
                data = match.groupdict()
                # Try to parse first/last name
                if 'name' in data:
                    data['name'] = data['name'].replace(',', '').replace('"', '')
                    components = re.split(r'\s+', data['name'], maxsplit=1, flags=re.UNICODE)
                    data['first_name'] = components[0]
                    if len(components) > 1:
                        data['last_name'] = components[1]
                u = FarmUser(username=data['login'],
                             first_name=data.get('first_name', ''),
                             last_name=data.get('last_name', ''),
                             contributions=data['contrib'],
                             uid=data['uid'],
                             email=data['email'])
                try:
                    u.full_clean(exclude=['password', 'first_name', 'last_name'])
                except ValidationError as e:
                    failed[data['login']] = e.message_dict
                else:
                    u.save()
                    success += 1
        self.stdout.write("User import: {}/{} successful".format(success, total))
        for (username, errors) in failed.items():
            self.stderr.write("Failure for user {}: {}".format(username, errors))
