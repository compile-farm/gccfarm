# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import sys

from django import forms
from django.db.models import Min
from django.shortcuts import render
from django.views import View
from django.views.generic import CreateView, ListView, FormView, TemplateView
from django.views.generic.base import RedirectView
from django.views.generic.detail import SingleObjectMixin
from django.views.decorators.csrf import csrf_protect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.contrib.sites.shortcuts import get_current_site
from django.contrib.admin.views.decorators import staff_member_required
from django.contrib import messages
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import PasswordResetForm
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.decorators import login_required

from .models import FarmUser
from .forms import SetEmailForm
from gccfarm_machine.models import Machine
from utils import send_templated_mail, send_templated_mail_managers


class CustomPasswordResetForm(PasswordResetForm):
    """Override the default password reset to allow resetting password for
    users with no usable password."""

    def get_users(self, email):
        UserModel = get_user_model()
        active_users = UserModel._default_manager.filter(**{
            '%s__iexact' % UserModel.get_email_field_name(): email,
            'is_active': True,
        })
        return list(active_users)


class EmailChangeView(FormView):
    form_class = SetEmailForm
    success_url = reverse_lazy('email_change_done')
    template_name = 'gccfarm_web/email_change_form.html'

    @method_decorator(csrf_protect)
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EmailChangeView, self).dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(EmailChangeView, self).get_form_kwargs()
        kwargs['user'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.save()
        return super(EmailChangeView, self).form_valid(form)


class EmailChangeDoneView(TemplateView):
    template_name = 'gccfarm_web/email_change_done.html'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(EmailChangeDoneView, self).dispatch(*args, **kwargs)


class FarmUserCreateForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        # Add CSS class to all field widgets
        super(FarmUserCreateForm, self).__init__(*args, **kwargs)
        for field_name, field in self.fields.items():
            field.widget.attrs['class'] = 'form-control'

    def save(self, *args, **kwargs):
        self.instance.approved = False
        return super(FarmUserCreateForm, self).save(*args, **kwargs)

    class Meta:
        model = FarmUser
        fields = ['username', 'first_name', 'last_name', 'email', 'contributions']


class FarmUserCreateView(CreateView):
    model = FarmUser
    template_name = 'gccfarm_web/join.html'
    success_url = reverse_lazy('join_confirmation')
    form_class = FarmUserCreateForm

    def form_valid(self, *args, **kwargs):
        # Send notification email to managers
        user = self.object
        current_site = get_current_site(self.request)
        email_context = {
            'user': user,
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'https' if self.request.is_secure() else 'http',
        }
        send_templated_mail_managers(email_context,
                                   "gccfarm_user/new_request/subject.txt",
                                   "gccfarm_user/new_request/email.txt",
                                   fail_silently=True)
        return super(FarmUserCreateView, self).form_valid(*args, **kwargs)


@method_decorator(staff_member_required, name='dispatch')
class ApprovedUserList(ListView):
    model = FarmUser
    template_name = 'gccfarm_web/approved_accounts.html'
    context_object_name = 'accounts'

    def get_queryset(self):
        return FarmUser.objects.filter(approved=True, approved_date__isnull=False).order_by('-approved_date')


@method_decorator(staff_member_required, name='dispatch')
class RejectedUserList(ListView):
    model = FarmUser
    template_name = 'gccfarm_web/rejected_accounts.html'
    context_object_name = 'accounts'

    def get_queryset(self):
        return FarmUser.objects.filter(approved=False, approved_date__isnull=False).order_by('-approved_date')


@method_decorator(staff_member_required, name='dispatch')
class AccountRequestList(ListView):
    model = FarmUser
    template_name = 'gccfarm_web/review_account_requests.html'
    context_object_name = 'accounts'

    def get_queryset(self):
        # Rejected users have approved=False, but a non-null approved date
        return FarmUser.objects.filter(approved=False, approved_date__isnull=True).order_by('request_date')

    def get_context_data(self, **kwargs):
        context = super(AccountRequestList, self).get_context_data(**kwargs)
        # Display the last few approved and rejected accounts
        context['approved_accounts'] = FarmUser.objects.filter(approved=True, approved_date__isnull=False).order_by('-approved_date')[:10]
        context['rejected_accounts'] = FarmUser.objects.filter(approved=False, approved_date__isnull=False).order_by('-approved_date')[:5]
        return context


class ApproveAccountForm(forms.Form):
    welcome_email = forms.BooleanField(required=False, initial=True,
                                       label="Send a welcome email with instructions to the new user")


@method_decorator(staff_member_required, name='dispatch')
class ApproveAccountRequest(SingleObjectMixin, FormView):
    model = FarmUser
    form_class = ApproveAccountForm
    template_name = 'gccfarm_web/approve_account_request.html'
    success_url = reverse_lazy('review_account_requests')
    context_object_name = 'user'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(ApproveAccountRequest, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(ApproveAccountRequest, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        user = self.object
        user.approve(self.request)
        messages.success(self.request,
                         'User "{}" approved, assigned UID: {}'.format(user.username, user.uid))
        if form.cleaned_data['welcome_email']:
            # Send email
            current_site = get_current_site(self.request)
            email_context = {
                'user': user,
                'domain': current_site.domain,
                'site_name': current_site.name,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'token': default_token_generator.make_token(user),
                'protocol': 'https' if self.request.is_secure() else 'http',
            }
            try:
                send_templated_mail(email_context, "gccfarm_user/approved_user/subject.txt",
                                    "gccfarm_user/approved_user/email.txt",
                                    None, [user.email])
            except:
                exc_type, exc_value, _ = sys.exc_info()
                error = "{}: {}".format(exc_type, exc_value)
                messages.error(self.request,
                               'Failed to send email to {}: {}'.format(user.email,
                                                                       error))
            else:
                messages.success(self.request,
                                 'Email sent to {}'.format(user.email))
        return super(ApproveAccountRequest, self).form_valid(form)


class RejectAccountForm(forms.Form):
    reject_reason = forms.CharField(required=True, widget=forms.Textarea)
    email_notification = forms.BooleanField(required=False, initial=True,
                                            label="Send email notification to user")


@method_decorator(staff_member_required, name='dispatch')
class RejectAccountRequest(SingleObjectMixin, FormView):
    model = FarmUser
    form_class = RejectAccountForm
    template_name = 'gccfarm_web/reject_account_request.html'
    success_url = reverse_lazy('review_account_requests')
    context_object_name = 'user'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(RejectAccountRequest, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(RejectAccountRequest, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        user = self.object
        reason = form.cleaned_data['reject_reason']
        user.reject(self.request, reason)
        messages.warning(self.request,
                         'User "{}" has been rejected'.format(user.username))
        if form.cleaned_data['email_notification']:
            email_context = {'user': user, 'reject_reason': reason}
            try:
                send_templated_mail(email_context, "gccfarm_user/rejected_user/subject.txt",
                                    "gccfarm_user/rejected_user/email.txt",
                                    None, [user.email])
            except:
                exc_type, exc_value, _ = sys.exc_info()
                error = "{}: {}".format(exc_type, exc_value)
                messages.error(self.request,
                               'Failed to send email to {}: {}'.format(user.email,
                                                                       error))
            else:
                messages.success(self.request,
                                 'Email sent to {}'.format(user.email))
        return super(RejectAccountRequest, self).form_valid(form)


@method_decorator(staff_member_required, name='dispatch')
class DeploymentDashboard(TemplateView):
    template_name = 'gccfarm_web/deployment_dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(DeploymentDashboard, self).get_context_data(**kwargs)
        last_user = FarmUser.objects.filter(uid__isnull=False).order_by('-uid')[0]
        agg = Machine.managed_machines.aggregate(Min('largest_deployed_uid'))
        min_deployed_uid = agg['largest_deployed_uid__min']
        # Various statistics
        context['shared_machines'] = Machine.objects.filter(shared=True, retired=False)
        context['nonshared_machines'] = Machine.objects.filter(shared=False, retired=False)
        context['retired_machines'] = Machine.objects.filter(retired=True)
        context['undeployed_users'] = FarmUser.objects.filter(deployment_started=False, uid__isnull=False)
        context['partial_machines'] = Machine.managed_machines.filter(largest_deployed_uid__lt=last_user.uid)
        context['partial_users'] = FarmUser.objects.filter(uid__gt=min_deployed_uid)
        context['no_uid_users'] = FarmUser.objects.filter(uid__isnull=True)
        context['users_in_sshkey_queue'] = FarmUser.objects.filter(sshkey_deploy_queue__isnull=False).distinct()
        context['users_in_sshkey_backlog'] = FarmUser.objects.filter(sshkey_deploy_backlog__isnull=False).distinct()
        context['machines_in_sshkey_backlog'] = Machine.managed_machines.filter(sshkey_deploy_backlog__isnull=False).distinct()
        return context
