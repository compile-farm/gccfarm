from django import forms


class SetEmailForm(forms.Form):
    """
    A form that lets a user change their email address
    """
    error_messages = {
        'email_mismatch': "The two email fields didn't match.",
    }
    new_email1 = forms.EmailField(label="New email address")
    new_email2 = forms.EmailField(label="New email address confirmation")

    def __init__(self, user, *args, **kwargs):
        self.user = user
        super(SetEmailForm, self).__init__(*args, **kwargs)

    def clean_new_email2(self):
        email1 = self.cleaned_data.get('new_email1')
        email2 = self.cleaned_data.get('new_email2')
        if email1 and email2:
            if email1 != email2:
                raise forms.ValidationError(
                    self.error_messages['email_mismatch'],
                    code='email_mismatch',
                )
        return email2

    def save(self, commit=True):
        email = self.cleaned_data["new_email1"]
        self.user.email = email
        if commit:
            self.user.save()
        return self.user
