# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import path, re_path

from gccfarm_user import views


urlpatterns = [
    path('email_change/', views.EmailChangeView.as_view(), name='email_change'),
    path('email_change_done/', views.EmailChangeDoneView.as_view(), name='email_change_done'),
    path('new/', views.FarmUserCreateView.as_view(), name='join'),
    path('review/all/', views.AccountRequestList.as_view(), name='review_account_requests'),
    path('review/all/approved/', views.ApprovedUserList.as_view(), name='approved_accounts'),
    path('review/all/rejected/', views.RejectedUserList.as_view(), name='rejected_accounts'),
    re_path(r'^review/approve/(?P<pk>[0-9]+)/$', views.ApproveAccountRequest.as_view(), name='approve_account_request'),
    re_path(r'^review/reject/(?P<pk>[0-9]+)/$', views.RejectAccountRequest.as_view(), name='reject_account_request'),
    path('dashboard/', views.DeploymentDashboard.as_view(), name='deployment_dashboard'),
]
