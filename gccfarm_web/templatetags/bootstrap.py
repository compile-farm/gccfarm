from django import template
from django.contrib.messages import constants as DEFAULT_MESSAGE_LEVELS

register = template.Library()


# Copied from django-bootstrap3 (Apache2 license)
MESSAGE_LEVEL_CLASSES = {
    DEFAULT_MESSAGE_LEVELS.DEBUG: "alert alert-warning",
    DEFAULT_MESSAGE_LEVELS.INFO: "alert alert-info",
    DEFAULT_MESSAGE_LEVELS.SUCCESS: "alert alert-success",
    DEFAULT_MESSAGE_LEVELS.WARNING: "alert alert-warning",
    DEFAULT_MESSAGE_LEVELS.ERROR: "alert alert-danger",
}

@register.filter
def bootstrap_message_classes(message):
    """
    Return the message classes for a message
    """
    extra_tags = None
    try:
        extra_tags = message.extra_tags
    except AttributeError:
        pass
    if not extra_tags:
        extra_tags = ""
    classes = [extra_tags]
    try:
        level = message.level
    except AttributeError:
        pass
    else:
        try:
            classes.append(MESSAGE_LEVEL_CLASSES[level])
        except KeyError:
            classes.append("alert alert-danger")
    return ' '.join(classes).strip()


# Autodetect bootstrap CSS class to use based on the message type.
@register.inclusion_tag('gccfarm_web/bootstrap_messages.html')
def bootstrap_messages(messages):
    return { "messages": messages }

@register.inclusion_tag('gccfarm_web/bootstrap_message.html')
def bootstrap_message(message):
    return { "message": message }

# Override CSS class (useful when displaying simple strings)
@register.inclusion_tag('gccfarm_web/bootstrap_messages.html')
def bootstrap_errors(messages):
    return { "messages": messages, "classes": "alert alert-danger" }

@register.inclusion_tag('gccfarm_web/bootstrap_message.html')
def bootstrap_error(message):
    return { "message": message, "classes": "alert alert-danger" }

@register.inclusion_tag('gccfarm_web/bootstrap_messages.html')
def bootstrap_infos(messages):
    return { "messages": messages, "classes": "alert alert-info" }

@register.inclusion_tag('gccfarm_web/bootstrap_message.html')
def bootstrap_info(message):
    return { "message": message, "classes": "alert alert-info" }
