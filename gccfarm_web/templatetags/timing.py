# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import template
from django.utils.safestring import mark_safe
from django.utils.translation import ngettext
from django.contrib.humanize.templatetags import humanize

register = template.Library()


@register.filter
def naturaltime_with_hover(datetime):
    """Same as naturaltime, but adds the absolute date and time as a hover"""
    res = '<span title="{}">{}</span>'.format(datetime.strftime("%d %B %Y, %H:%M"),
                                              humanize.naturaltime(datetime))
    return mark_safe(res)

@register.filter
def naturaldelta_days(delta):
    """Same as humanizelib.naturaldelta, but express the timedelta in days whenever possible"""
    seconds = abs(delta.seconds)
    days = abs(delta.days)
    if days < 1:
        if seconds == 0:
            return ""
        elif seconds < 60:
            return ngettext("%d second", "%d seconds", seconds) % seconds
        elif seconds < 3600:
            minutes = seconds // 60
            return ngettext("%d minute", "%d minutes", minutes) % minutes
        else:
            hours = seconds // 3600
            return ngettext("%d hour", "%d hours", hours) % hours
    else:
        return ngettext("%d day", "%d days", days) % days
