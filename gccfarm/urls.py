from django.urls import include, path
from django.contrib import admin
from django.contrib.auth import views as auth_views

from gccfarm_user import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('machines/', include('gccfarm_machine.urls')),
    path('users/', include('gccfarm_user.urls')),
    # Override this specific view to allow resetting password for users without password
    path('password_reset/',
        auth_views.PasswordResetView.as_view(form_class=views.CustomPasswordResetForm),
        name='password_reset'),
    path('', include('django.contrib.auth.urls')),
    path('', include('gccfarm_web.urls')),
]
