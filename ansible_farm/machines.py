# -*- coding: utf-8 -*-
from __future__ import unicode_literals, print_function

""" Use ansible to gather information about managed machines: CPU type,
memory, OS, etc...

"""

import sys
import json
import datetime

import django.utils.timezone
from ansible.plugins.callback import CallbackBase

from gccfarm_machine.models import Machine
from ansible_farm.boilerplate import run_play


CPU_FIELDS = ('nb_cpu', 'nb_cores', 'nb_threads')

# Fields that can be managed manually: if Ansible fails to obtain
# meaningful data, we should keep the value already in the database.
# However, these fields are still overriden if Ansible returns something.
MANUAL_FIELDS = CPU_FIELDS + ('processor',)


class ResultCallback(CallbackBase):
    QUIET = False
    CALLBACK_VERSION = 2.0
    CALLBACK_NAME = 'gccfarm_machines'
    CALLBACK_TYPE = 'aggregate'

    def __init__(self):
      super(ResultCallback, self).__init__()
      self.ansible_facts = dict()

    def print(self, msg):
        if not self.QUIET:
            self._display.display(msg)

    def print_stderr(self, msg):
        self._display.display(msg, stderr=True)

    def get_lscpu(self, facts, key):
        """
        lscpu facts is a list of key-value pairs, with possibly multiple entries for the same.

        This function returns the list of values associated with a given key.
        """
        return [v for (k, v) in filter(lambda pair: pair[0] == key, facts)]

    def parse_facts(self, facts):
        """Parse facts provided by ansible.  Returns a dictionary mapping variable
        name to variable value.  Each value is guaranteed to be either a string
        or an integer.
        """
        res = dict()
        for fact in (
                'ansible_architecture', 'ansible_machine', # Often identical (but sometimes not)
                'ansible_system', # Linux, AIX, NetBSD...
                'ansible_system_vendor', # Dell... (sometimes not relevant at all)
                'ansible_product_name',
                # os_family is generally too broad (e.g. 'Debian' for Ubuntu)
                'ansible_os_family', 'ansible_distribution', 'ansible_distribution_release',
                'ansible_distribution_version', 'ansible_distribution_major_version', 'ansible_distribution_minor_version',
                #'ansible_lsb', # Often not present
                'ansible_kernel',
                'ansible_userspace_bits',
                'ansible_memtotal_mb', 'ansible_swaptotal_mb',
                # processor_count and processor_vcpus are sometimes 0 (EdgeRouter)
                'ansible_processor_count', 'ansible_processor_cores', 'ansible_processor_threads_per_core', 'ansible_processor_vcpus',
                'ansible_processor', # Often a list, sometimes a string
                'ansible_mounts', # Dictionary
                'ansible_uptime_seconds',
        ):
            try:
                value = facts[fact]
            except KeyError:
                continue
            # Parse a few special cases
            if fact == 'ansible_mounts':
                # Useful for debugging
                res[fact] = value
                for mountpoint in value:
                    # AIX has no 'size_total' field, and Solaris machines
                    # may have a /home mountpoint with a size equal to 0.
                    if 'size_total' not in mountpoint or mountpoint['size_total'] == 0:
                        continue
                    if mountpoint['mount'] == '/home' or mountpoint['mount'] == '/export/home':
                        res['mount_home_size_total'] = mountpoint['size_total']
                        res['mount_home_size_available'] = mountpoint['size_available']
                        res['mount_home_fstype'] = mountpoint['fstype']
                    if mountpoint['mount'] == '/':
                        res['mount_root_size_total'] = mountpoint['size_total']
                        res['mount_root_size_available'] = mountpoint['size_available']
                        res['mount_root_fstype'] = mountpoint['fstype']
            elif fact == 'ansible_processor':
                # Useful for debugging
                res[fact] = value
                # Processor is often a list
                if not isinstance(value, str if sys.version_info[0] >= 3 else basestring):
                    value = set(value)
                    if len(value) > 1:
                        # Remove useless information
                        value = { proc for proc in value if "AuthenticAMD" not in proc and "GenuineIntel" not in proc }
                        # Remove CPU core ID
                        value = { proc for proc in value if not proc.isnumeric() }
                    # Just concatenate remaining items, in the unlikely
                    # case that there is still more than one
                    value = ' + '.join(value)
                # Note that we may override this value below if lscpu returns data
                res['processor'] = value
            elif fact in ('ansible_processor_count', 'ansible_processor_cores', 'ansible_processor_threads_per_core', 'ansible_processor_vcpus'):
                # General case is integer, but they are strings on BSD and OS X.
                if not isinstance(value, int):
                    try:
                        value = int(value)
                    except ValueError:  # Sometimes a variable is "NA" (NetBSD)
                        value = 0
                res[fact] = value
            else:
                # Simply copy fact
                res[fact] = value

        # Handle OS
        version = res.get('ansible_distribution_version', '')
        # NetBSD used to have a super-long version string (FIXME: check if still the case)
        if len(version) > 20:
            version = ''
        # Debian has inconsistent version reporting: "9.X" up to
        # stretch, but only "10" starting from buster.
        # See https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=931197
        # and https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=939733
        # Try to reconstruct the correct "major.minor" format for all OSes.
        major_version = res.get('ansible_distribution_major_version', '')
        if version != '' and version == major_version:
            # "version" has no minor information.  Try to add it if available.
            if 'ansible_distribution_minor_version' in res:
                version += '.' + res['ansible_distribution_minor_version']
        # NetBSD can have identical "version" and "release" properties
        release = res.get('ansible_distribution_release', '')
        if version == release:
            release = ''
        res['os'] = '{} {} {}'.format(res.get('ansible_distribution', ''),
                                      version, release)

        # Handle CPU
        if not isinstance(res.get('ansible_processor_cores', 1), int):
            res['ansible_processor_cores'] = 1
        # Ansible on OpenBSD is buggy, it reports each core as a CPU. Assume single socket.
        if res['ansible_system'] == 'OpenBSD':
            res['ansible_processor_count'] = 1
        # Compute total number of CPU sockets/cores/threads, using first
        # hwloc, then lscpu, then the usual cpuinfo-based Ansible facts.
        if 'hwloc' in facts and facts['hwloc']:
            res['nb_cpu'] = facts['hwloc']['package']
            if res['nb_cpu'] == None:
                res['nb_cpu'] = 1
            res['nb_cores'] = facts['hwloc']['core']
            res['nb_threads'] = facts['hwloc']['pu']
            res['cpu_source'] = 'hwloc'
        elif 'lscpu' in facts and facts['lscpu']:
            lscpu = facts['lscpu']
            # Main source for number of threads
            threads = self.get_lscpu(lscpu, "CPU(s)")
            if len(threads) == 0:
                # BSD style
                threads = self.get_lscpu(lscpu, "Total CPU(s)")
            if len(threads) == 0:
                self.print_stderr("Warning: could not determine total number of threads from lscpu data (check locale or install hwloc)")
                res['nb_threads'] = 0
            else:
                res['nb_threads'] = int(threads[0])
            sockets = self.get_lscpu(lscpu, "Socket(s)")
            cores_per_socket = self.get_lscpu(lscpu, "Core(s) per socket")
            threads_per_core = self.get_lscpu(lscpu, "Thread(s) per core")
            # Handle multiple CPU models (such as on Apple M1)
            # For each CPU, we should have the three fields
            if len(sockets) != len(cores_per_socket) or len(cores_per_socket) != len(threads_per_core):
                self.print_stderr("Problem when parsing lscpu data: inconsistent number of fields. Continuing anyway...")
            res['nb_cpu'] = 0
            res['nb_cores'] = 0
            nb_threads = 0
            for i in range(len(sockets)):
                res['nb_cpu'] += int(sockets[i])
                res['nb_cores'] += int(sockets[i]) * int(cores_per_socket[i])
                # This is less reliable, so don't use it, just check consistency
                nb_threads += int(sockets[i]) * int(cores_per_socket[i]) * int(threads_per_core[i])
            # If we don't have any other data, let's use the less reliable option
            if res['nb_threads'] == 0:
                res['nb_threads'] = nb_threads
            if len(sockets) == 0:
                self.print_stderr("Warning: no cores/sockets information in lscpu, using default values")
                # By default, assume 1 socket and no hyper-threading
                # This happens on gcc231
                res['nb_cpu'] = 1
                res['nb_cores'] = res['nb_threads']
                nb_threads = res['nb_threads']
            if res['nb_threads'] != nb_threads:
                self.print_stderr("Warning: inconsistent total number of threads in lscpu data ({} vs {})".format(res['nb_threads'], nb_threads))
            res['cpu_source'] = 'lscpu'
        else:
            res['nb_cpu'] = res.get('ansible_processor_count', 1)
            res['nb_cores'] = res.get('ansible_processor_cores', 1) * res['nb_cpu']
            res['nb_threads'] = res.get('ansible_processor_threads_per_core', 1) * res['nb_cores']
            res['cpu_source'] = 'cpuinfo'

        # Use CPU model name from lscpu if available (often of better
        # quality than cpuinfo or equivalent)
        if 'lscpu' in facts and facts['lscpu']:
            lscpu = facts['lscpu']
            vendors = self.get_lscpu(lscpu, "Vendor ID")
            vendors = [v for v in vendors if v not in ["GenuineIntel", "AuthenticAMD"]]
            models = self.get_lscpu(lscpu, "Model name")
            if len(models) > 0:
                model = " + ".join(models)
                if len(vendors) > 0:
                    # Avoid repetition
                    if vendors[0] not in model:
                        model = vendors[0] + " " + model
                res["processor"] = model
            # In one case (gcc185), the "BIOS Model name" is the correct
            # data while "Model name" is wrong.  Overwrite (except if QEMU, see below).
            bios_vendors = self.get_lscpu(lscpu, "BIOS Vendor ID")
            bios_models = self.get_lscpu(lscpu, "BIOS Model name")
            if len(bios_models) > 0:
                model = bios_models[0]
                if len(bios_vendors) > 0:
                    # Avoid repetition
                    if bios_vendors[0] not in model:
                        model = bios_vendors[0] + " " + model
                # For QEMU VMs, the "BIOS Model name" shows a made-up CPU
                # model, which is not what we want.  In that case, keep
                # the regular name.
                if "qemu" in model.lower():
                    pass
                else:
                    res["processor"] = model

        # Handle uptime
        res['uptime'] = datetime.timedelta(seconds=res.get('ansible_uptime_seconds', 0))
        return res

    def update_database(self, hostname, variables):
        try:
            m = Machine.objects.get(hostname=hostname)
        except Machine.DoesNotExist:
            self.print_stderr('Error fetching machine {} from database'.format(hostname))
            return
        # Hack to fetch default values for all fields: create an empty Machine without saving it.
        empty_machine = Machine()

        def update_field(field, var_name):
            # Don't touch CPU counts at all if we know Ansible is wrong
            if field in CPU_FIELDS and m.manual_cpu_count:
                return
            # For fields that can be managed manually, if the value is not
            # provided by Ansible or is empty, then don't touch the existing
            # value.
            if field in MANUAL_FIELDS and not variables.get(var_name, ""):
                return
            # If a variable is not provided by Ansible, reset to default value.
            # This avoids keeping obsolete data in the database.
            default_value = getattr(empty_machine, field)
            value = variables.get(var_name, default_value)
            setattr(m, field, value)

        update_field('architecture', 'ansible_architecture')
        update_field('kernel', 'ansible_kernel')
        update_field('os', 'os')
        update_field('processor', 'processor')
        update_field('nb_cpu', 'nb_cpu')
        update_field('nb_cores', 'nb_cores')
        update_field('nb_threads', 'nb_threads')
        update_field('memtotal_mb', 'ansible_memtotal_mb')
        update_field('swaptotal_mb', 'ansible_swaptotal_mb')
        update_field('mount_root_fstype', 'mount_root_fstype')
        update_field('mount_root_size_total', 'mount_root_size_total')
        update_field('mount_root_size_available', 'mount_root_size_available')
        update_field('mount_home_fstype', 'mount_home_fstype')
        update_field('mount_home_size_total', 'mount_home_size_total')
        update_field('mount_home_size_available', 'mount_home_size_available')
        update_field('uptime', 'uptime')
        m.success = True
        m.last_success = django.utils.timezone.now()
        try:
            m.save()
        except (AttributeError, ValueError) as e:
            self.print_stderr('Error saving details for machine {}'.format(hostname))

    def v2_runner_on_ok(self, result, **kwargs):
        host = result._host
        # Accumulate initial facts
        if host.name not in self.ansible_facts:
            self.ansible_facts[host.name] = result._result['ansible_facts']
        else:
            self.ansible_facts[host.name].update(result._result['ansible_facts'])
            # We should now have all facts
            self.print("\n== {} ==".format(host.name))
            facts = self.ansible_facts[host.name]
            # Use this to display all available raw facts
            #self.print(json.dumps({host.name: facts}, indent=4))
            variables = self.parse_facts(facts)
            # Simply display all known variables for now
            for (key, value) in variables.items():
                self.print('{}: {}'.format(key, value))
            self.update_database(host.name, variables)

    def v2_runner_on_failed(self, result, **kwargs):
        host = result._host
        m = Machine.objects.get(hostname=host.name)
        m.success = False
        m.save()
        self.print("\n==== Failed {}".format(host))
        self.print(json.dumps({host.name: result._result}, indent=4))

    def v2_runner_on_unreachable(self, result, **kwargs):
        host = result._host
        m = Machine.objects.get(hostname=host.name)
        m.success = False
        m.save()
        self.print("\n==== Unreachable {}".format(host))
        self.print(json.dumps({host.name: result._result}, indent=4))


def gather_info(machines=None, include_retired=False, quiet=False):
    ResultCallback.QUIET = quiet
    if machines == None or machines == []:
        if include_retired:
            hosts = list(Machine.objects.all())
        else:
            hosts = list(Machine.objects.filter(retired=False))
    else:
        hosts = list(Machine.objects.filter(name__in=machines))
    play = dict(
        name = "Gather information",
        hosts = 'all',
        gather_facts = 'yes',
        tasks = [dict(action=dict(module='expert_cpu_facts'),
                      name='Collect expert CPU facts')]
    )
    run_play(hosts, play, callback_class=ResultCallback)
