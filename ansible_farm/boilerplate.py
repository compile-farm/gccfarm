from collections import namedtuple
import os

from django.conf import settings
from ansible.parsing.dataloader import DataLoader
from ansible.vars.manager import VariableManager
from ansible.inventory.manager import InventoryManager
from ansible.playbook.play import Play
from ansible.executor.task_queue_manager import TaskQueueManager
from ansible.plugins.callback import CallbackBase
from ansible.module_utils.common.collections import ImmutableDict
from ansible import context


def run_play(machines, play_source, callback_class=None, check=False):
    # This really needs to be a list and not a queryset
    assert(isinstance(machines, list))

    # Local custom modules
    custom_module_path = os.path.join(settings.BASE_DIR, "ansible_farm", "modules")

    # since the API is constructed for CLI it expects certain options to always be set in the context object
    context.CLIARGS = ImmutableDict(connection='ssh', module_path=[custom_module_path], forks=8, become=None,
                                    become_method=None, become_user=None, check=check, diff=check, verbosity=0)

    # initialize needed objects
    loader = DataLoader()

    # Instantiate callback class for handling results as they come in
    if callback_class != None:
        callback = callback_class()
    else:
        callback = None

    # create inventory and pass to var manager
    hosts_list = [m.hostname for m in machines]
    sources = ",".join(hosts_list)
    if len(hosts_list) == 1:
        sources += ","
    inventory = InventoryManager(loader=loader, sources=sources)
    variable_manager = VariableManager(loader=loader, inventory=inventory)

    # Set variables such as SSH port or username
    for m in machines:
        host = inventory.get_host(m.hostname)
        host.set_variable('ansible_port', m.first_ssh_port())
        host.set_variable('ansible_ssh_user', m.ssh_admin_user)
        host.set_variable('ansible_python_interpreter', m.python_interpreter)
        # Custom variables
        host.set_variable('cfarm_home_directory', m.home_directory)

    play = Play().load(play_source, variable_manager=variable_manager, loader=loader)

    # actually run it
    tqm = None
    try:
        tqm = TaskQueueManager(
            inventory=inventory,
            variable_manager=variable_manager,
            loader=loader,
            passwords=None,
            stdout_callback=callback,
          )
        result = tqm.run(play)
    finally:
        if tqm is not None:
            tqm.cleanup()
